from datetime import datetime as dt, time, timedelta
import logging

from .constants import (
    START_DATES,
    FALL_START,
    SPRING_START,
    SUMMER_START,
    SUMMER_2_START,
    )


class InvalidSummerDateError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)


def in_boundary(date):
    """Works for undergrads non-WS ONLY"""

    month = dt.strftime(date, '%m')
    day = int(dt.strftime(date, '%d'))

    if month in ['05', '08', '12']:
        return True
    elif month == '01' and day < 16:
        return True
    else:
        return False


def determine_semester(date, in_boundary=False, work_study=False):
    month = dt.strftime(date, '%m')
    day = int(dt.strftime(date, '%d'))

    if in_boundary:
        spring = ['12', '01', '02', '03', '04']
        summer = ['05', '06', '07']
        fall = ['08', '09', '10', '11']
    else:
        spring = ['01', '02', '03', '04', '05']
        summer = ['06', '07', '08']
        fall = ['09', '10', '11', '12']

    if month in spring:
        sem = 2
    if month in summer:
        sem = 6
    if month in fall:
        sem = 9

    if month == '01':
        if work_study:
            sem = 2
        elif day < 16 and not in_boundary:
            sem = 9

    if month == '08' and work_study:
        if day < 16:
            sem = 6
        else:
            sem = 9

    return sem


def determine_summer_semester(start_date, ccyys_to_check):
    if ccyys_to_check[-1] != '6':
        raise InvalidSummerDateError()

    year = ccyys_to_check[:4]
    summer_2 = dt.strptime(year+SUMMER_2_START, '%Y%m/%d')

    if start_date < summer_2:
        return '6-1'
    else:
        return '6-2'


def determine_current_semester():

    return determine_ccyys(dt.now())


def determine_ccyys(date, in_boundary=False, work_study=False):
    year = int(dt.strftime(date, '%Y'))
    month = dt.strftime(date, '%m')
    current_year = int(dt.strftime(dt.now(), '%Y'))
    sem = determine_semester(date, in_boundary, work_study)
    spring = dt.strptime(str(year)+SPRING_START, '%Y%m/%d')
    new_year = dt.strptime(str(year)+'1/1', '%Y%m/%d')

    if sem == 9:
        if year == current_year + 1:
            year = current_year
        elif new_year <= date < spring:
            year -= 1

    if sem ==2:
        if month == '12':
            year += 1

    return str(year) + str(sem)


def jobs_overlap(addl_job, job):
    addl_start = addl_job['start_date'].replace(hour=0, minute=0)
    addl_end = addl_job['end_date'].replace(hour=0, minute=0)
    job_start = job.start_date.replace(hour=0, minute=0)
    job_end = job.end_date.replace(hour=0, minute=0)

    if (addl_start >= job_start and addl_start < job_end):
        return True
    elif (addl_start < job_start and addl_end >= job_start):
        return True
    else:
        return False

