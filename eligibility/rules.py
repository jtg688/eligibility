from datetime import datetime as dt
from django.core.exceptions import ImproperlyConfigured

try:
    from django.conf import settings
    BATCH = settings.BATCH
except ImproperlyConfigured:
    BATCH = True

from .constants import (
    FALL_START,
    JOB_FAMILIES,
    LONG_SEM_CREDIT_HOURS,
    SPRING_START,
    SUMMER_START,
    WS_FULL_TIME,
    WS_NA_SPRING_START,
    )
from .date_helper import (
    determine_current_semester,
    jobs_overlap,
    )


CURRENT_SEM = int(determine_current_semester()[-1])


def old_enough(age, required_age=15):
    if age < required_age:
        return 'Student is not old enough to be hired.', 'crit'


def correct_status(student, job):
    if job.job_profile[0] == 'U':
        req_status = 'undergrad'
    elif job.job_profile[0] == 'G':
        req_status = 'grad'
    elif job.job_profile[0] == 'W':
        return
    else:
        raise Exception

    if student.grad_status != req_status:
        return ('Job requires {0}, student is {1}.'.format(
                req_status, student.grad_status
            ), 'crit')


def student_is_enrolled(student, job):
    msg = None
    sem = job.ccyys_to_check[-1]
    prev = student.prev_sem_status
    current = student.current_sem_status
    ws = job.job_family == JOB_FAMILIES['ws']

    if student.current_sem_status == '1':
        raise Exception

    if job.time_type == WS_FULL_TIME:
        return

    if sem == '6' and current in ['8', '9'] and prev in ['8', '9']:
        msg = 'No longer enrolled.'
    elif sem == '6' and ws and current in ['8', '9']:
        msg = 'No longer enrolled.'
    elif sem != '6' and current in ['8', '9']:
        msg = 'No longer enrolled.'

    if msg:
        return msg, 'crit'


def gpa_met(gpa, required_gpa=2.0):
    """
    If GPA is 0, assume first semester student, and waive GPA requirements
    """
    if gpa == 0:
        return
    if gpa < required_gpa:
        return 'GPA requirements not met.', 'crit'


def credit_hours_met(student, job):

    if job.ccyys_to_check[-1] is '6':
        return summer_credit_hours_met(
            student,
            job,
            )
    else:
        return long_sem_credit_hours_met(student, job)


def long_sem_credit_hours_met(student, job, long_sem_hours=None):
    criticality = 'non-crit'
    today = dt.now()
    msg = None
    credit_hours = 0

    if long_sem_hours is not None:
        credit_hours = long_sem_hours
    else:
        credit_hours = LONG_SEM_CREDIT_HOURS[job.job_profile][0]

    if student.current_sem_hours < credit_hours:
        msg = '{0} credit hours required, but student only has {1}.'.format(
                credit_hours, student.current_sem_hours
            )

    if today >= student.hr_deadline:
        criticality = 'crit'

    if msg:
        return msg, criticality


def summer_credit_hours_met(student, job):
    criticality = 'non-crit'
    today = dt.now()
    msg = None

    if job.job_family == JOB_FAMILIES['undergrad_aca_nt']:
        spring_enrollment = 12
        summer_enrollment = 3
    elif job.job_family == JOB_FAMILIES['undergrad_non_aca']:
        spring_enrollment = 6
        summer_enrollment = 3

    if CURRENT_SEM is 2:
        if (student.prev_sem_hours < spring_enrollment and
            student.current_sem_hours < summer_enrollment):
            msg = ('If student is not enrolled in {0} or more hours this '
                'spring, they must be enrolled in a minimum of {1} hours '
                'this summer.').format(
                    spring_enrollment,
                    summer_enrollment,
                    )

    if CURRENT_SEM is 6:
        if (student.prev_sem_hours < spring_enrollment and
            student.current_sem_hours < summer_enrollment):
            msg = ('Student was not enrolled in {0} or more hours in the '
                'spring, so they must be enrolled in a minimum of {1} hours '
                'this summer.').format(
                    spring_enrollment,
                    summer_enrollment,
                    )

    if today >= student.hr_deadline:
        criticality = 'crit'

    if msg:
        return msg, criticality


def ws_credit_hours_met(
    student,
    job,
    long_sem_hours=0,
    spring_hours=0,
    summer_hours=0):

    if job.ccyys_to_check[-1] == '6':
        return ws_summer_credit_hours_met(
            student,
            job,
            spring_hours,
            summer_hours,
            )
    else:
        return long_sem_credit_hours_met(student, job, long_sem_hours)


def ws_summer_credit_hours_met(student, job, spring_hours=0, summer_hours=0):
    criticality = 'non-crit'
    today = dt.now()
    msg = None

    if CURRENT_SEM is 2 and spring_hours:
        if (student.prev_sem_hours < spring_hours and
            student.current_sem_hours < summer_hours):
            msg = ('If enrollment is less than {0} hours this spring, then '
                'enrollment must be at least {1} hours this summer.').format(
                    spring_hours,
                    summer_hours,
                    )

    elif CURRENT_SEM is 6 and spring_hours:
        if (student.prev_sem_hours < spring_hours and
            student.current_sem_hours < summer_hours):
            msg = ('Enrollment was less than {0} this spring, so enrollment '
                'must be at least {1} hours this summer.').format(
                    spring_hours,
                    summer_hours,
                    )

    else:
        if student.current_sem_hours < summer_hours:
            msg = '{0} credit hours required, but student only has {1}.'.format(
                summer_hours,
                student.current_sem_hours
            )

    if today >= student.hr_deadline:
        criticality = 'crit'

    if msg:
        return msg, criticality


def max_scheduled_hours_per_citizenship(student, job):
    if student.is_us_citizen():
        return max_scheduled_work_hours(job, 40)
    else:
        return max_scheduled_work_hours(job, 20)


def max_scheduled_work_hours(job, maximum=0):

    if not maximum:
        maximum = job.max_work_hours

    if job.scheduled_hours > maximum:
        msg = 'Student cannot be scheduled over {0} hours for this job.'.format(
            maximum
            )
        return msg, 'crit'


def total_scheduled_work_hours(job, student):
    """Check total hours for all jobs combined"""
    total_hours = 0
    msgs = []
    statuses = []
    other_jobs = []

    job_sem = job.ccyys_to_check[-1]

    for addl_job in student.additional_jobs:
        if (job.position_id != addl_job['position_id'] and
            jobs_overlap(addl_job, job)):
            other_jobs.append(addl_job)

    for other_job in other_jobs:
        total_hours += other_job['scheduled_hours']

    total_hours += job.scheduled_hours

    if job_sem == '6':
        job_total_work_hours = 40
    elif (not student.is_us_citizen()
        and job.job_family == 'Undergraduate Student Non-Academic'):
        job_total_work_hours = 20
    else:
        job_total_work_hours = job.total_work_hours

    if total_hours > job_total_work_hours:
        msg = 'Student cannot work over {0} hours for {1}.'.format(
            job_total_work_hours,
            job.job_profile,
            )
        msgs.append(msg)

        if job.is_in_boundary and total_hours <= 40:
            statuses.append('non-crit')
        else:
            statuses.append('crit')

    for other_job in other_jobs:
        maximum = 0
        if job_sem == '6':
            maximum = 40

        if other_job['job_profile'][0] == 'G':
            if not BATCH:
                if total_hours > 20:
                    msg = ('This job may conflict with another job already held '
                        'by the student - {0}. Please review with the Grad '
                        'School.'.format(other_job['job_profile'])
                        )
                    msgs.append(msg)
                    statuses.append('non-crit')
            continue

        if not maximum:
            maximum = other_job['total_work_hours']

        if total_hours > maximum:
            msg = 'Student cannot work over {0} hours for {1}.'.format(
                maximum,
                other_job['job_profile']
                )
            msgs.append(msg)

            if job.is_in_boundary and total_hours <= 40:
                statuses.append('non-crit')
            else:
                statuses.append('crit')

    if msgs:
        return msgs, statuses


def quantity_of_work(student, job):
    """Check total hours for jobs and classes combined"""
    addl_hours = 0

    for addl_job in job.additional_jobs:
        if addl_job['position_id'] == job.position_id:
            continue
        if jobs_overlap(addl_job, job):
            addl_hours += addl_job['scheduled_hours']

    if addl_hours + student.current_sem_hours + job.scheduled_hours > 40:
        msg = ('Total hours scheduled (classes and work combined) is greater '
            'than 40.')
        return msg, 'non-crit'


def is_degree_seeking(student):
    if not student.degree_seeking:
        return 'Student is non-degree seeking.', 'crit'


def post_graduation(student, job, min_enrollment=6, allow_grad_students=False):
    criticality = 'non-crit'
    msg = None
    today = dt.now()

    if student.is_grad_student() and not allow_grad_students:
        msg = 'Grad students may not hold this position after graduation.'
        return msg, 'crit'

    if (job.ccyys_to_check[-1] == '6' and
        student.prev_sem_hours < min_enrollment):
        msg = ('Student must be enrolled in at least {0} hours the spring '
            'before graduating. Student is only enrolled in {1}.').format(
                min_enrollment,
                student.prev_sem_hours,
                )

    if today >= student.hr_deadline:
        criticality = 'crit'

    if msg:
        return msg, criticality


def sap_status(student):
    criticality = 'non-crit'
    msg = None
    today = dt.now()

    if not student.work_study['ok_to_disburse']:
        msg = student.work_study['disbursement_msg']

    if today >= student.hr_deadline:
        criticality = 'crit'

    if msg:
        return msg, criticality


def ws_check(student):
    if student.work_study['ws_error_returned']:
        return student.work_study['disbursement_msg'], 'crit'


def ws_ft_dates_valid(job):
    job_year = job.start_date.strftime('%Y')
    next_year = str(int(job_year) + 1)
    last_year = str(int(job_year) - 1)

    start_date = job.start_date.replace(
        hour=0,
        minute=0,
        second=0,
        microsecond=0,
        )
    end_date = job.end_date.replace(hour=0, minute=0, second=0, microsecond=0)

    SCH_YEAR_RANGE_A = (
        dt.strptime(FALL_START + '/' + job_year, '%m/%d/%Y'),
        dt.strptime('5/31/' + next_year, '%m/%d/%Y'),
        )
    SCH_YEAR_RANGE_B = (
        dt.strptime(FALL_START + '/' + last_year, '%m/%d/%Y'),
        dt.strptime('5/31/' + job_year, '%m/%d/%Y'),
        )

    windows = [SCH_YEAR_RANGE_A, SCH_YEAR_RANGE_B]

    for window in windows:
        if (start_date >= window[0] and start_date <= window[1]
            or end_date >= window[0] and end_date <= window[1]):
            msg = ('This is an invalid date range for a full-time Work-Study '
                'job.')
            return msg, 'crit'


def lingering_position(job):
    if job.end_date < dt.now():
        msg = ('Student job is active beyond the current End Employment Date '
        'of the position. Please review and process an End Additional Job or '
        'Termination.')
        return msg, 'crit'

