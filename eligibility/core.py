import logging

from django.conf import settings

from .constants import (
    JOB_FAMILIES,
    WS_STATE_EX_CODE_ACA,
    WS_STATE_EX_CODE_NON_ACA,
    )
from .validators import (
    GSJobValidator,
    UGNAValidator,
    UGANTValidator,
    WSAcaValidator,
    WSNonAcaValidator,
    )


class MFError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)


class InvalidJobProfileError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)


class InvalidStudentError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)


def determine_eligibility(job, student):
    eligibility = {
        'eligible': 0,
        'warning_level': None,
        'msg': None
    }
    logging.debug('Checking eligibility for {0} for job {1}.'.format(
        student.student_id, job.job_profile,
        ))

    if student.is_on_exception_table():
        eligibility['eligible'] = 1
        eligibility['warning_level'] = ['non-crit']
        eligibility['msg'] = ['Student is on exception table.']

    elif job.job_profile[0] == 'G':
        validator = GSJobValidator(job, student)
        validator.validate_assignment()

        eligibility['eligible'] = validator.eligibility
        eligibility['warning_level'] = validator.statuses
        eligibility['msg'] = validator.messages

    elif job.job_family == JOB_FAMILIES['undergrad_non_aca']:
        validator = UGNAValidator(job, student)
        validator.validate_assignment()

        eligibility['eligible'] = validator.eligibility
        eligibility['warning_level'] = validator.statuses
        eligibility['msg'] = validator.messages

    elif job.job_family == JOB_FAMILIES['undergrad_aca_nt']:
        validator = UGANTValidator(job, student)
        validator.validate_assignment()

        eligibility['eligible'] = validator.eligibility
        eligibility['warning_level'] = validator.statuses
        eligibility['msg'] = validator.messages

    elif job.job_family == JOB_FAMILIES['ws']:
        if job.job_classification == WS_STATE_EX_CODE_ACA:
            validator = WSAcaValidator(job, student)
            validator.validate_assignment()

        elif job.job_classification == WS_STATE_EX_CODE_NON_ACA:
            validator = WSNonAcaValidator(job, student)
            validator.validate_assignment()

        else:
            logging.error('Invalid work study job classification.')
            raise InvalidJobProfileError(
                '{0} is not a valid work study job classification.'.format(
                job.job_classification
                ))

        eligibility['eligible'] = validator.eligibility
        eligibility['warning_level'] = validator.statuses
        eligibility['msg'] = validator.messages

    else:
        logging.error('Invalid job family.')
        raise InvalidJobProfileError('{0} is not a valid job family.'.format(
            job.job_family
            ))

    logging.debug(eligibility)

    return eligibility

