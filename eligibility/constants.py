START_DATES = {
    'UG_FALL' : '09/01',
    'UG_EARLY_FALL' : '08/01',
    'UG_SPRING' : '01/16',
    'UG_EARLY_SPRING' : '12/01',
    'UG_SUMMER' : '06/01',
    'UG_EARLY_SUMMER' : '05/01',
    'UG_SUMMER_2' : '07/16',
}

FALL_START = '9/1'

SPRING_START = '1/16'

SUMMER_START = '6/1'

SUMMER_2_START = '7/16'

WS_NA_SPRING_START = '1/1'

"""
For the purposes of eligibility, students with a citizenship status of 0
(US citizen) or 3 (permanent resident) are considered domestic (D). Students
with a status of 2 (non-US citizen) are considered foreign (F).
"""
RESIDENCY = {'0': 'D', '2': 'F', '3': 'D'}

JOB_FAMILIES = {
    'undergrad_aca_nt': 'Undergraduate Student Academic',
    'undergrad_non_aca': 'Undergraduate Student Non-Academic',
    'ws': 'Work-Study',
    }

JOB_FAMILY_MAP = {
    'S_Work_Study' : 'Work-Study',
    'SU_Undergraduate_Student_Non_Academic' : ('Undergraduate Student '
        'Non-Academic'),
    'RU_Undergraduate_Student_Academic_Non_Teaching' : ('Undergraduate '
         'Student Academic'),
    }

DEFAULT_HOURS_STATUS = {'pre_deadline': 'non-crit', 'post_deadline': 'crit'}

WS_STATE_EX_CODE_ACA = ('Student Academic - Non-Teaching - (State Expenditure '
    + 'Classification)')

WS_STATE_EX_CODE_NON_ACA = ('Student Non-Academic - (State Expenditure '
    + 'Classification)')

WS_FULL_TIME = 'Full Time Work-Study - (Full Time Work-Study)'

JOB_CLASS_IDS = {
    'non_aca': ('STATE_EXPENDITURE_CLASSIFICATION_STUDENT_NON'
        + '_ACADEMIC'),
    'aca': ('STATE_EXPENDITURE_CLASSIFICATION_STUDENT_ACADEMIC_NON'
        + '_TEACHING'),
    'full_time': 'WORK-STUDY_FULL-TIME'
    }

JOB_CLASSIFICATION = {
    JOB_CLASS_IDS['non_aca']: WS_STATE_EX_CODE_NON_ACA,
    JOB_CLASS_IDS['aca']: WS_STATE_EX_CODE_ACA,
    JOB_CLASS_IDS['full_time']: WS_FULL_TIME,
    }

MAX_WORK_HOURS = {
    'aca': 20,
    'non_aca': 40,
    'non_aca_foreign': 20,
    'ws_aca': 19,
    'ws_aca_ft': 40,
    'ws_non_aca': 19,
    'ws_non_aca_ft': 40,
    }

TOTAL_WORK_HOURS = {
    'aca': 20,
    'non_aca': 40,
    'non_aca_foreign': 20,
    'ws_aca': 20,
    'ws_aca_ft': 40,
    'ws_non_aca': 40,
    'ws_non_aca_ft': 40,
    }

QUANTITY_OF_WORK = {
    'aca': 40,
    'non_aca': 40,
    'non_aca_foreign': 40,
    'ws_aca': 40,
    'ws_aca_ft': 40,
    'ws-non_aca': 40,
    'ws_non_aca_ft': 40,
    }

LONG_SEM_CREDIT_HOURS = {
    'U0074': [6,  DEFAULT_HOURS_STATUS],
    'U0075': [6,  DEFAULT_HOURS_STATUS],
    'U0076': [6,  DEFAULT_HOURS_STATUS],
    'U0077': [6,  DEFAULT_HOURS_STATUS],
    'U0073': [6,  DEFAULT_HOURS_STATUS],
    'U0078': [6,  DEFAULT_HOURS_STATUS],
    'U0079': [6,  DEFAULT_HOURS_STATUS],
    'U0098': [6,  DEFAULT_HOURS_STATUS],
    'U0073S': [6,  DEFAULT_HOURS_STATUS],
    'U0098S': [6,  DEFAULT_HOURS_STATUS],
    'U0066': [12,  DEFAULT_HOURS_STATUS],
    'U0070': [12,  DEFAULT_HOURS_STATUS],
    'U0095': [12,  DEFAULT_HOURS_STATUS],
    }

SUMMER_CREDIT_HOURS = {
    'U0074': [6, DEFAULT_HOURS_STATUS],
    'U0075': [6, DEFAULT_HOURS_STATUS],
    'U0076': [6, DEFAULT_HOURS_STATUS],
    'U0077': [6, DEFAULT_HOURS_STATUS],
    'U0073': [
        6, DEFAULT_HOURS_STATUS
        ],
    'U0078': [6, {'pre16': 'non-crit', 'post16': 'crit'}],
    'U0079': [
        6, {'pre16': 'non-crit', 'post16': 'crit'}
        ],
    'U0098': [
        6, {'pre16': 'non-crit', 'post16': 'crit'}
        ],
    }

UGNA_JOBS = [
    'U0074',
    'U0075',
    'U0076',
    'U0077',
    'U0073',
    'U0078',
    'U0079',
    'U0098',
    'U0073S',
    'U0098S',
    ]

UGANT_JOBS = [
    'U0066',
    'U0070',
    'U0095',
    ]

