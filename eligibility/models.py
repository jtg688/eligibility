from datetime import datetime as dt
import logging

from django.conf import settings

from .core import InvalidStudentError
from .constants import (
    MAX_WORK_HOURS,
    RESIDENCY,
    TOTAL_WORK_HOURS,
    WS_FULL_TIME,
    WS_STATE_EX_CODE_ACA,
    WS_STATE_EX_CODE_NON_ACA
    )
from .date_helper import determine_ccyys, in_boundary


class JobData(object):
    def __init__(self):
        self.student_id = self.set_student_id()
        self.job_family = self.set_job_family()
        self.position_id = self.set_position_id()
        self.job_classification = self.set_job_classification()
        self.time_type = self.set_time_type()
        self.scheduled_hours = self.set_scheduled_hours()
        self.start_date = self.set_start_date()
        self.end_date = self.set_end_date()
        self.is_in_boundary = self.set_is_in_boundary()
        self.ccyys_to_check = self.set_ccyys_to_check()
        self.max_work_hours = self.set_max_work_hours(
            self.job_family,
            self.job_classification,
            self.time_type,
            )[0]
        self.total_work_hours = self.set_max_work_hours(
            self.job_family,
            self.job_classification,
            self.time_type,
            )[1]
        self.previous_appointments = self.set_previous_appointments()

    def set_student_id(self):
        raise NotImplementedError

    def set_job_family(self):
        raise NotImplementedError

    def set_position_id(self):
        raise NotImplementedError

    def set_job_classification(self):
        raise NotImplementedError

    def set_time_type(self):
        raise NotImplementedError

    def set_scheduled_hours(self):
        raise NotImplementedError

    def set_start_date(self):
        raise NotImplementedError

    def set_end_date(self):
        raise NotImplementedError

    def set_is_in_boundary(self):
        if self.job_family in [
            'Undergraduate Student Academic',
            'Undergraduate Student Non-Academic',
            ]:
            starts_in = in_boundary(self.start_date)
            ends_in = in_boundary(self.end_date)

            if starts_in and not ends_in:
                return True
            if starts_in and ends_in:
                start_m = dt.strftime(self.start_date, '%m')
                start_yr = int(dt.strftime(self.start_date, '%Y'))
                end_d = int(dt.strftime(self.end_date, '%d'))
                end_m = dt.strftime(self.end_date, '%m')
                end_yr = int(dt.strftime(self.end_date, '%Y'))

                if start_m == end_m and start_yr == end_yr:
                    return False
                elif (start_m == '12' and end_m == '01'
                    and (start_yr + 1 == end_yr)
                    and end_d < 16):
                    return False
                else:
                    return True

        else:
            return False

    def set_ccyys_to_check(self):
        work_study = self.job_family == 'Work-Study'

        if self.is_retro_end:
            return determine_ccyys(self.end_date, work_study=work_study)
        if self.is_future_hire:
            return determine_ccyys(
                self.start_date,
                self.is_in_boundary,
                work_study=work_study,
                )
        else:
            start_m = dt.strftime(self.start_date, '%m')
            start_yr = int(dt.strftime(self.start_date, '%Y'))
            now_m = dt.strftime(dt.now(), '%m')
            now_yr = int(dt.strftime(dt.now(), '%Y'))

            if start_m == now_m and start_yr == now_yr:
                return determine_ccyys(
                    dt.now(),
                    self.is_in_boundary,
                    work_study=work_study,
                    )
            else:
                return determine_ccyys(dt.now(), work_study=work_study)

    def set_ajobs(self):
        raise NotImplementedError

    def set_max_work_hours(self, job_family, job_class, time_type):
        max_work_hours = 0
        total_work_hours = 0

        if job_family == 'Undergraduate Student Non-Academic':
            max_work_hours = MAX_WORK_HOURS['non_aca']
            total_work_hours = TOTAL_WORK_HOURS['non_aca']
        elif job_family == 'Undergraduate Student Academic':
            max_work_hours = MAX_WORK_HOURS['aca']
            total_work_hours = TOTAL_WORK_HOURS['aca']
        elif job_family == 'Work-Study':
            if job_class == WS_STATE_EX_CODE_ACA:
                if time_type == WS_FULL_TIME:
                    max_work_hours = MAX_WORK_HOURS['ws_aca_ft']
                    total_work_hours = TOTAL_WORK_HOURS['ws_aca_ft']
                else:
                    max_work_hours = MAX_WORK_HOURS['ws_aca']
                    total_work_hours = TOTAL_WORK_HOURS['ws_aca']
            else:
                if time_type == WS_FULL_TIME:
                    max_work_hours = MAX_WORK_HOURS['ws_non_aca_ft']
                    total_work_hours = TOTAL_WORK_HOURS['ws_non_aca_ft']
                else:
                    max_work_hours = MAX_WORK_HOURS['ws_non_aca']
                    total_work_hours = TOTAL_WORK_HOURS['ws_non_aca']

        return max_work_hours, total_work_hours

    def set_previous_appointments(self):
        raise NotImplementedError

    @property
    def is_retro_start(self):
        today = dt.now().replace(hour=0, minute=0, second=0, microsecond=0)
        start_date = self.start_date.replace(
            hour=0,
            minute=0,
            second=0,
            microsecond=0,
            )
        return start_date < today

    @property
    def is_retro_end(self):
        today = dt.now().replace(hour=0, minute=0, second=0, microsecond=0)
        end_date = self.end_date.replace(
            hour=0,
            minute=0,
            second=0,
            microsecond=0,
            )
        return end_date < today

    @property
    def is_future_hire(self):
        today = dt.now().replace(hour=0, minute=0, second=0, microsecond=0)
        start_date = self.start_date.replace(
            hour=0,
            minute=0,
            second=0,
            microsecond=0,
            )
        return start_date > today


class StudentData(object):
    def __init__(self, pda, job_data):
        self.job_data = job_data
        self.student_id = job_data.student_id
        self.additional_jobs = job_data.additional_jobs

        mf_data = self.load_mainframe_data(pda, job_data)

        self.hr_deadline = mf_data['hr_deadline']
        self.grad_status = mf_data['grad_status']
        self.birthdate = mf_data['birthdate']
        self.gpa = mf_data['gpa']
        self.residency = mf_data['residency']
        self.current_sem_hours = mf_data['current_sem_hours']
        self.current_sem_status = mf_data['current_sem_status']
        self.prev_sem_hours = mf_data['prev_sem_hours']
        self.prev_sem_status = mf_data['prev_sem_status']
        self.fall_hours = mf_data['fall_hours']
        self.fall_status = mf_data['fall_status']
        self.graduation_date = mf_data['graduation_date']
        self.degree_seeking = mf_data['degree_seeking']
        self.student_exception = mf_data['student_exception']
        self.grad_data = mf_data['grad_data']
        self.work_study = mf_data['work_study']

        self.age = self.set_age()

        self.update_addl_job_max_by_citizenship()

    def load_mainframe_data(self, pda, job_data):
        mf_data = {
            'hr_deadline': None,
            'grad_status': 'undergrad',
            'birthdate': '',
            'gpa': 0,
            'residency': 'US',
            'current_sem_hours': 0,
            'current_sem_status': '',
            'prev_sem_hours': 0,
            'prev_sem_status': '',
            'fall_hours': 0,
            'fall_status': '',
            'graduation_date': '',
            'degree_seeking': False,
            'student_exception': False,
            'will_return_for_fall': False,
            'grad_data':{'eligible': False, 'failed_audits': []},
            'work_study': {
                'ws_error_returned': False,
                'ok_to_disburse': False,
                'disbursement_msg': ''
                },
        }


        try:
            mf_data['hr_deadline'] = dt.strptime(
                pda.recv.semester_data.hr_deadline, '%m%d%Y'
                )
        except ValueError as e:
            logging.info(e)
            raise e

        common_fields = pda.recv.common_fields
        if common_fields.graduate_student == True:
            mf_data['grad_status'] = 'grad'
        try:
            mf_data['birthdate'] = dt.strptime(
                common_fields.birthdate_ccyymmdd, '%Y%m%d'
                )
        except ValueError as e:
            logging.info(e)
            logging.error(
                'No birthdate returned for this student - {0}'.format(
                    self.student_id
                ))
            raise InvalidStudentError(
                'Invalid birthdate returned - {0}.'.format(mf_data['birthdate'])
                )

        mf_data['gpa'] = common_fields.cumulative_gpa
        mf_data['residency'] = self.set_residency(common_fields.citizenship)
        mf_data['current_sem_hours'] = common_fields.current_semester_hours
        mf_data['current_sem_status'] = common_fields.current_semester_status
        mf_data['prev_sem_hours'] = common_fields.previous_semester_hours
        mf_data['prev_sem_status'] = common_fields.previous_semester_status
        mf_data['fall_hours'] = common_fields.fall_semester_hours
        mf_data['fall_status'] = common_fields.fall_semester_status
        try:
            mf_data['graduation_date'] = dt.strptime(
                common_fields.graduation_date, '%Y%m%d'
                )
        except ValueError as e:
            logging.info(e)
        if common_fields.degree_seeking:
            mf_data['degree_seeking'] = True
        if common_fields.student_exception:
            mf_data['student_exception'] = True
        if common_fields.will_return_for_fall:
            mf_data['will_return_for_fall'] = True

        grad_fields = pda.recv.grad_student_fields
        if grad_fields.eligible_for_position:
            mf_data['grad_data']['eligible'] = True
        mf_data['grad_data']['failed_audits'] = grad_fields.failed_audit_array

        ws_fields = pda.recv.work_study_fields
        if ws_fields.ws_error_returned:
            mf_data['work_study']['ws_error_returned'] = True
        if ws_fields.ok_to_disburse:
            mf_data['work_study']['ok_to_disburse'] = True
        mf_data['work_study']['disbursement_msg'] = ws_fields.disbursement_msg

        return mf_data

    def set_age(self):
        today = dt.today()
        if settings.DEBUG and self.student_id in settings.UNDER_AGE:
            return 14
        return today.year - self.birthdate.year - (
            (today.month, today.day) < (self.birthdate.month, self.birthdate.day)
            )

    def set_residency(self, citizenship):
        return RESIDENCY[citizenship]

    def is_grad_student(self):
        return self.grad_status is 'grad'

    def is_undergrad(self):
        return self.grad_status is 'undergrad'

    def is_us_citizen(self):
        return self.residency is 'D'

    def is_on_exception_table(self):
        return self.student_exception

    def has_additional_jobs(self):
        return bool(self.additional_jobs)

    def update_addl_job_max_by_citizenship(self):
        non_aca_fams = [
            'SU_Undergraduate_Student_Non_Academic',
            'Undergraduate Student Non-Academic',
            ]
        if not self.is_us_citizen():
            for ajob in self.additional_jobs:
                if ajob['job_family'] in non_aca_fams:
                    ajob['max_work_hours'] = 20
                    ajob['total_work_hours'] = 20

