from datetime import datetime as dt, timedelta
from unittest import TestCase, mock

from .data import Student, Job, GSFailedAudit
from eligibility.constants import WS_FULL_TIME
import eligibility.validators as vals


class TestJobValidator(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.is_retro_start = False
        self.job.end_date = dt.now() + timedelta(days=1)
        self.student = Student()
        self.student.age = 18
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)

    def test_validate_assignment(self):
        validator = vals.JobValidator(self.job, self.student)
        self.assertRaises(NotImplementedError, validator.validate_assignment)

    def test_run_rules(self):
        validator = vals.JobValidator(self.job, self.student)
        self.assertRaises(NotImplementedError, validator.run_rules)

    def test_set_criticality_critical(self):
        validator = vals.JobValidator(self.job, self.student)
        validator.statuses = ['non-crit', 'crit', 'non-crit']
        validator.set_criticality()
        self.assertEqual(validator.criticality, 'Critical')
        validator.statuses = []

    def test_set_criticality_warning(self):
        validator = vals.JobValidator(self.job, self.student)
        validator.statuses = ['non-crit', 'non-crit', 'non-crit']
        validator.set_criticality()
        self.assertEqual(validator.criticality, 'Warning')
        validator.statuses = []

    def test_set_criticality_none(self):
        validator = vals.JobValidator(self.job, self.student)
        validator.set_criticality()
        self.assertIsNone(validator.criticality)

    def test_set_eligibility_warning(self):
        validator = vals.JobValidator(self.job, self.student)
        validator.criticality = 'Warning'
        validator.set_eligibility()
        self.assertEqual(validator.eligibility, 0)
        validator.criticality = None

    def test_set_eligibility_critical(self):
        validator = vals.JobValidator(self.job, self.student)
        validator.criticality = 'Critical'
        validator.set_eligibility()
        self.assertEqual(validator.eligibility, 0)
        validator.criticality = None

    def test_set_eligibility_eligible(self):
        validator = vals.JobValidator(self.job, self.student)
        validator.set_eligibility()
        self.assertEqual(validator.eligibility, 1)


class TestGSJobValidator(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.is_retro_start = False
        self.job.end_date = dt.now() + timedelta(days=1)
        self.student = Student()
        self.student.age = 18
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)

    def test_run_rules_no_msgs(self):
        self.student.grad_data = {'eligible':True, 'failed_audits':[]}
        validator = vals.GSJobValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])

    def test_run_rules_msgs(self):
        self.student.grad_data = {
            'eligible':True,
            'failed_audits':[GSFailedAudit(True, 1), GSFailedAudit(False, 2)],
            }
        validator = vals.GSJobValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(
            validator.messages,
            ['Failed GS audit 1.', 'Failed GS audit 2.'],
            )

    def test_run_rules_retro_batch(self):
        self.job.is_retro_start = True
        self.student.grad_data = {'eligible':True, 'failed_audits':[]}
        validator = vals.GSJobValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        self.job.is_retro_start = False

    def test_run_rules_retro_not_batch(self):
        vals.BATCH = False
        self.job.is_retro_start = True
        self.student.grad_data = {'eligible':True, 'failed_audits':[]}
        validator = vals.GSJobValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(len(validator.messages), 1)
        self.assertIn('in the past', validator.messages[0])
        self.job.is_retro_start = False
        vals.BATCH = True


class TestUGJobValidator(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.is_retro_start = False
        self.job.end_date = dt.now() + timedelta(days=1)
        self.student = Student()
        self.student.age = 18
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)

    def test_run_rules_no_msgs(self):
        validator = vals.UGJobValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])

    def test_run_rules_msgs(self):
        vals.old_enough = mock.Mock(
            return_value=('Not old enough.', 'crit')
            )
        vals.correct_status = mock.Mock(
            return_value=('Incorrect status.', 'crit')
            )
        vals.student_is_enrolled = mock.Mock(
            return_value=('Not enrolled.', 'crit')
            )
        validator = vals.UGJobValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(len(validator.messages), 3)
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)

    def test_run_rules_retro_batch(self):
        self.job.is_retro_start = True
        validator = vals.UGJobValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        self.job.is_retro_start = False

    def test_run_rules_retro_not_batch(self):
        vals.BATCH = False
        self.job.is_retro_start = True
        validator = vals.UGJobValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(len(validator.messages), 1)
        self.assertIn('in the past', validator.messages[0])
        self.job.is_retro_start = False
        vals.BATCH = True

    def test_run_rules_lingering_batch(self):
        self.job.end_date = dt.now() - timedelta(days=2)
        validator = vals.UGJobValidator(self.job, self.student)
        validator.validate_assignment()
        self.assertEqual(len(validator.messages), 1)
        self.assertIn(
            'beyond the current End Employment Date',
            validator.messages[0]
            )
        self.job.end_date = dt.now() + timedelta(days=1)

    def test_run_rules_lingering_batch_only_result(self):
        self.job.end_date = dt.now() - timedelta(days=2)
        self.student.age = 10
        validator = vals.UGJobValidator(self.job, self.student)
        validator.validate_assignment()
        self.assertEqual(len(validator.messages), 1)
        self.assertIn(
            'beyond the current End Employment Date',
            validator.messages[0]
            )
        self.job.end_date = dt.now() + timedelta(days=1)
        self.student.age = 18

    def test_run_rules_lingering_not_batch(self):
        vals.BATCH = False
        self.job.end_date = dt.now() - timedelta(days=2)
        validator = vals.UGJobValidator(self.job, self.student)
        validator.validate_assignment()
        self.assertEqual(validator.messages, [])
        self.job.end_date = dt.now() + timedelta(days=1)
        vals.BATCH = True

    def test_validate_assignment_ineligible(self):
        vals.old_enough = mock.Mock(
            return_value=('Not old enough.', 'crit')
            )
        vals.correct_status = mock.Mock(
            return_value=('Incorrect status.', 'crit')
            )
        vals.student_is_enrolled = mock.Mock(
            return_value=('Not enrolled.', 'crit')
            )
        validator = vals.UGJobValidator(self.job, self.student)
        validator.validate_assignment()
        self.assertEqual(validator.eligibility, 0)
        self.assertEqual(validator.criticality, 'Critical')
        self.assertEqual(len(validator.messages), 3)
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)

    def test_validate_assignment_eligible(self):
        validator = vals.UGJobValidator(self.job, self.student)
        validator.validate_assignment()
        self.assertEqual(validator.eligibility, 1)
        self.assertIsNone(validator.criticality)
        self.assertEqual(validator.messages, [])


class TestUGNAValidator(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.is_retro_start = False
        self.job.start_date = dt.now()
        self.job.end_date = dt.now() + timedelta(days=1)
        self.job.ccyys_to_check = '20189'
        self.student = Student()
        self.student.age = 18
        self.student.gpa = 3.0
        self.student.graduation_date = dt.now() + timedelta(days=10)
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)
        vals.post_graduation = mock.Mock(return_value=None)
        vals.credit_hours_met = mock.Mock(return_value=None)
        vals.max_scheduled_hours_per_citizenship = mock.Mock(return_value=None)
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)
        vals.quantity_of_work = mock.Mock(return_value=None)

    def test_post_grad_not_post_grad(self):
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        vals.credit_hours_met = mock.Mock(
            return_value=('not post grad', 'crit')
            )
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['not post grad'])
        vals.post_graduation = mock.Mock(return_value=None)

    def test_post_grad_no_grad_date(self):
        self.student.graduation_date = None
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.post_graduation = mock.Mock(return_value=None)
        self.student.graduation_date = dt.now() + timedelta(days=10)

    def test_post_grad_post_grad_ok(self):
        self.job.start_date = self.student.graduation_date + timedelta(days=5)
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        self.job.start_date = dt.now()

    def test_post_grad_post_grad_fail(self):
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        self.job.start_date = self.student.graduation_date + timedelta(days=5)
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['post grad'])
        self.job.start_date = dt.now()
        vals.post_graduation = mock.Mock(return_value=None)

    def test_citizenship_summer(self):
        self.job.ccyys_to_check = '20186'
        vals.max_scheduled_hours_per_citizenship = mock.Mock(
            return_value=('citizenship fail', 'crit')
            )
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.max_scheduled_hours_per_citizenship = mock.Mock(return_value=None)
        self.job.ccyys_to_check = '20189'

    def test_citizenship_not_summer(self):
        vals.max_scheduled_hours_per_citizenship = mock.Mock(
            return_value=('citizenship fail', 'crit')
            )
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['citizenship fail'])
        vals.max_scheduled_hours_per_citizenship = mock.Mock(return_value=None)

    def test_total_scheduled_work_hours(self):
        vals.total_scheduled_work_hours = mock.Mock(
            return_value=(['too many hours'], ['crit'])
            )
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too many hours'])
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)

    def test_quantity_of_work_batch_no_msgs(self):
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.quantity_of_work = mock.Mock(return_value=None)

    def test_quantity_of_work_batch_msgs(self):
        vals.total_scheduled_work_hours = mock.Mock(
            return_value=(['too many hours'], ['crit'])
            )
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too many hours', 'too much work'])
        vals.quantity_of_work = mock.Mock(return_value=None)
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)

    def test_quantity_of_work_not_batch(self):
        vals.BATCH = False
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.UGNAValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too much work'])
        vals.quantity_of_work = mock.Mock(return_value=None)
        vals.BATCH = True


class TestUGANTValidator(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.is_retro_start = False
        self.job.start_date = dt.now()
        self.job.end_date = dt.now() + timedelta(days=1)
        self.job.ccyys_to_check = '20189'
        self.student = Student()
        self.student.age = 18
        self.student.gpa = 3.0
        self.student.graduation_date = dt.now() + timedelta(days=10)
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)
        vals.post_graduation = mock.Mock(return_value=None)
        vals.credit_hours_met = mock.Mock(return_value=None)
        vals.gpa_met = mock.Mock(return_value=None)
        vals.max_scheduled_work_hours = mock.Mock(return_value=None)
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)
        vals.is_degree_seeking = mock.Mock(return_value=None)
        vals.quantity_of_work = mock.Mock(return_value=None)

    def test_post_grad_not_post_grad(self):
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        vals.credit_hours_met = mock.Mock(
            return_value=('not post grad', 'crit')
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['not post grad'])
        vals.post_graduation = mock.Mock(return_value=None)

    def test_post_grad_no_grad_date(self):
        self.student.graduation_date = None
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.post_graduation = mock.Mock(return_value=None)
        self.student.graduation_date = dt.now() + timedelta(days=10)

    def test_post_grad_post_grad_ok(self):
        self.job.start_date = self.student.graduation_date + timedelta(days=5)
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        self.job.start_date = dt.now()

    def test_post_grad_post_grad_fail(self):
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        self.job.start_date = self.student.graduation_date + timedelta(days=5)
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['post grad'])
        self.job.start_date = dt.now()
        vals.post_graduation = mock.Mock(return_value=None)

    def test_gpa_met(self):
        vals.gpa_met = mock.Mock(
            return_value=('gpa not met', 'crit')
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['gpa not met'])
        vals.gpa_met = mock.Mock(return_value=None)

    def test_max_scheduled_work_hours_summer(self):
        self.job.ccyys_to_check = '20186'
        vals.max_scheduled_work_hours = mock.Mock(
            return_value=('max hours fail', 'crit')
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['max hours fail'])
        vals.max_scheduled_work_hours = mock.Mock(return_value=None)
        self.job.ccyys_to_check = '20189'

    def test_max_scheduled_work_hours_not_summer(self):
        vals.max_scheduled_work_hours = mock.Mock(
            return_value=('max hours fail', 'crit')
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['max hours fail'])
        vals.max_scheduled_work_hours = mock.Mock(return_value=None)

    def test_is_degree_seeking(self):
        vals.is_degree_seeking = mock.Mock(
            return_value=('not degree seeking', 'crit')
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['not degree seeking'])
        vals.is_degree_seeking = mock.Mock(return_value=None)

    def test_total_scheduled_work_hours(self):
        vals.total_scheduled_work_hours = mock.Mock(
            return_value=(['too many hours'], ['crit'])
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too many hours'])
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)

    def test_quantity_of_work_batch_no_msgs(self):
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.quantity_of_work = mock.Mock(return_value=None)

    def test_quantity_of_work_batch_msgs(self):
        vals.total_scheduled_work_hours = mock.Mock(
            return_value=(['too many hours'], ['crit'])
            )
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too many hours', 'too much work'])
        vals.quantity_of_work = mock.Mock(return_value=None)
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)

    def test_quantity_of_work_not_batch(self):
        vals.BATCH = False
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.UGANTValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too much work'])
        vals.quantity_of_work = mock.Mock(return_value=None)
        vals.BATCH = True

class TestWSValidator(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.is_retro_start = False
        self.job.start_date = dt.now()
        self.job.end_date = dt.now() + timedelta(days=1)
        self.job.ccyys_to_check = '20189'
        self.job.time_type = ''
        self.student = Student()
        self.student.age = 18
        self.student.gpa = 3.0
        self.student.graduation_date = dt.now() + timedelta(days=10)
        self.student.is_undergrad = mock.Mock(return_value=True)
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)
        vals.post_graduation = mock.Mock(return_value=None)
        vals.ws_credit_hours_met = mock.Mock(return_value=None)
        vals.gpa_met = mock.Mock(return_value=None)
        vals.sap_status = mock.Mock(return_value=None)
        vals.ws_check = mock.Mock(return_value=None)
        vals.max_scheduled_work_hours = mock.Mock(return_value=None)
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)
        vals.is_degree_seeking = mock.Mock(return_value=None)
        vals.quantity_of_work = mock.Mock(return_value=None)

    def test_is_degree_seeking(self):
        vals.is_degree_seeking = mock.Mock(
            return_value=('not degree seeking', 'crit')
            )
        validator = vals.WSValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['not degree seeking'])
        vals.is_degree_seeking = mock.Mock(return_value=None)

    def test_sap_status(self):
        vals.sap_status = mock.Mock(
            return_value=('bad sap', 'crit')
            )
        validator = vals.WSValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['bad sap'])
        vals.sap_status = mock.Mock(return_value=None)

    def test_ws_check(self):
        vals.ws_check = mock.Mock(
            return_value=('check failed', 'crit')
            )
        validator = vals.WSValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['check failed'])
        vals.ws_check = mock.Mock(return_value=None)

    def test_max_scheduled_work_hours_summer(self):
        self.job.ccyys_to_check = '20186'
        vals.max_scheduled_work_hours = mock.Mock(
            return_value=('max hours fail', 'crit')
            )
        validator = vals.WSValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['max hours fail'])
        vals.max_scheduled_work_hours = mock.Mock(return_value=None)
        self.job.ccyys_to_check = '20189'

    def test_max_scheduled_work_hours_not_summer(self):
        vals.max_scheduled_work_hours = mock.Mock(
            return_value=('max hours fail', 'crit')
            )
        validator = vals.WSValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['max hours fail'])
        vals.max_scheduled_work_hours = mock.Mock(return_value=None)

    def test_total_scheduled_work_hours(self):
        vals.total_scheduled_work_hours = mock.Mock(
            return_value=(['too many hours'], ['crit'])
            )
        validator = vals.WSValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too many hours'])
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)

    def test_ws_ft_dates_valid_not_ft(self):
        vals.ws_ft_dates_valid = mock.Mock(
            return_value=('invalid ws ft dates', 'crit')
            )
        validator = vals.WSValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.ws_ft_dates_valid = mock.Mock(return_value=None)

    def test_ws_ft_dates_valid_ft_good_dates(self):
        self.job.time_type = WS_FULL_TIME
        validator = vals.WSValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        self.job.time_type = ''

    def test_ws_ft_dates_valid_ft_bad_dates(self):
        self.job.time_type = WS_FULL_TIME
        vals.ws_ft_dates_valid = mock.Mock(
            return_value=('invalid ws ft dates', 'crit')
            )
        validator = vals.WSValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['invalid ws ft dates'])
        vals.ws_ft_dates_valid = mock.Mock(return_value=None)
        self.job.time_type = ''


class TestWSAcaValidator(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.is_retro_start = False
        self.job.start_date = dt.now()
        self.job.end_date = dt.now() + timedelta(days=1)
        self.job.ccyys_to_check = '20189'
        self.job.time_type = ''
        self.student = Student()
        self.student.age = 18
        self.student.gpa = 3.0
        self.student.graduation_date = dt.now() + timedelta(days=10)
        self.student.is_undergrad = mock.Mock(return_value=True)
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)
        vals.post_graduation = mock.Mock(return_value=None)
        vals.ws_credit_hours_met = mock.Mock(return_value=None)
        vals.gpa_met = mock.Mock(return_value=None)
        vals.sap_status = mock.Mock(return_value=None)
        vals.ws_check = mock.Mock(return_value=None)
        vals.max_scheduled_work_hours = mock.Mock(return_value=None)
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)
        vals.is_degree_seeking = mock.Mock(return_value=None)
        vals.quantity_of_work = mock.Mock(return_value=None)

    def test_gpa_met_undergrad(self):
        vals.gpa_met = mock.Mock(
            return_value=('gpa not met', 'crit')
            )
        validator = vals.WSAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['gpa not met'])
        vals.gpa_met = mock.Mock(return_value=None)

    def test_gpa_met_grad(self):
        self.student.is_undergrad = mock.Mock(return_value=False)
        vals.gpa_met = mock.Mock(
            return_value=('gpa not met', 'crit')
            )
        validator = vals.WSAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['gpa not met'])
        vals.gpa_met = mock.Mock(return_value=None)
        self.student.is_undergrad = mock.Mock(return_value=True)

    def test_post_grad_no_grad_date(self):
        self.student.graduation_date = None
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        validator = vals.WSAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.post_graduation = mock.Mock(return_value=None)
        self.student.graduation_date = dt.now() + timedelta(days=10)

    def test_post_grad_post_grad_ok(self):
        self.job.start_date = self.student.graduation_date + timedelta(days=5)
        validator = vals.WSAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        self.job.start_date = dt.now()

    def test_post_grad_post_grad_fail(self):
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        self.job.start_date = self.student.graduation_date + timedelta(days=5)
        validator = vals.WSAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['post grad'])
        self.job.start_date = dt.now()
        vals.post_graduation = mock.Mock(return_value=None)

    def test_ws_credit_hours_met(self):
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        vals.ws_credit_hours_met = mock.Mock(
            return_value=('not post grad', 'crit')
            )
        validator = vals.WSAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['not post grad'])
        vals.post_graduation = mock.Mock(return_value=None)

    def test_quantity_of_work_batch_no_msgs(self):
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.WSAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.quantity_of_work = mock.Mock(return_value=None)

    def test_quantity_of_work_batch_msgs(self):
        vals.total_scheduled_work_hours = mock.Mock(
            return_value=(['too many hours'], ['crit'])
            )
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.WSAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too many hours', 'too much work'])
        vals.quantity_of_work = mock.Mock(return_value=None)
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)

    def test_quantity_of_work_not_batch(self):
        vals.BATCH = False
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.WSAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too much work'])
        vals.quantity_of_work = mock.Mock(return_value=None)
        vals.BATCH = True


class TestWSNonAcaValidator(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.is_retro_start = False
        self.job.start_date = dt.now()
        self.job.end_date = dt.now() + timedelta(days=1)
        self.job.ccyys_to_check = '20189'
        self.job.time_type = ''
        self.student = Student()
        self.student.age = 18
        self.student.gpa = 3.0
        self.student.graduation_date = dt.now() + timedelta(days=10)
        self.student.is_undergrad = mock.Mock(return_value=True)
        vals.old_enough = mock.Mock(return_value=None)
        vals.correct_status = mock.Mock(return_value=None)
        vals.student_is_enrolled = mock.Mock(return_value=None)
        vals.post_graduation = mock.Mock(return_value=None)
        vals.ws_credit_hours_met = mock.Mock(return_value=None)
        vals.gpa_met = mock.Mock(return_value=None)
        vals.sap_status = mock.Mock(return_value=None)
        vals.ws_check = mock.Mock(return_value=None)
        vals.max_scheduled_work_hours = mock.Mock(return_value=None)
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)
        vals.is_degree_seeking = mock.Mock(return_value=None)
        vals.quantity_of_work = mock.Mock(return_value=None)

    def test_post_grad_no_grad_date(self):
        self.student.graduation_date = None
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        validator = vals.WSNonAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.post_graduation = mock.Mock(return_value=None)
        self.student.graduation_date = dt.now() + timedelta(days=10)

    def test_post_grad_post_grad_ok(self):
        self.job.start_date = self.student.graduation_date + timedelta(days=5)
        validator = vals.WSNonAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        self.job.start_date = dt.now()

    def test_post_grad_post_grad_fail(self):
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        self.job.start_date = self.student.graduation_date + timedelta(days=5)
        validator = vals.WSNonAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['post grad'])
        self.job.start_date = dt.now()
        vals.post_graduation = mock.Mock(return_value=None)

    def test_ws_credit_hours_met_ug(self):
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        vals.ws_credit_hours_met = mock.Mock(
            return_value=('not post grad', 'crit')
            )
        validator = vals.WSNonAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['not post grad'])
        vals.post_graduation = mock.Mock(return_value=None)

    def test_ws_credit_hours_met_grad(self):
        self.student.is_undergrad = mock.Mock(return_value=False)
        vals.post_graduation = mock.Mock(
            return_value=('post grad', 'crit')
            )
        vals.ws_credit_hours_met = mock.Mock(
            return_value=('not post grad', 'crit')
            )
        validator = vals.WSNonAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['not post grad'])
        vals.post_graduation = mock.Mock(return_value=None)
        self.student.is_undergrad = mock.Mock(return_value=True)

    def test_quantity_of_work_batch_no_msgs(self):
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.WSNonAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, [])
        vals.quantity_of_work = mock.Mock(return_value=None)

    def test_quantity_of_work_batch_msgs(self):
        vals.total_scheduled_work_hours = mock.Mock(
            return_value=(['too many hours'], ['crit'])
            )
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.WSNonAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too many hours', 'too much work'])
        vals.quantity_of_work = mock.Mock(return_value=None)
        vals.total_scheduled_work_hours = mock.Mock(return_value=None)

    def test_quantity_of_work_not_batch(self):
        vals.BATCH = False
        vals.quantity_of_work = mock.Mock(
            return_value=('too much work', 'crit')
            )
        validator = vals.WSNonAcaValidator(self.job, self.student)
        validator.run_rules()
        self.assertEqual(validator.messages, ['too much work'])
        vals.quantity_of_work = mock.Mock(return_value=None)
        vals.BATCH = True

