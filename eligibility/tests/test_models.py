from datetime import datetime as dt, timedelta
from unittest import TestCase, mock

from .data import Student, Job, MF_DATA, TestSettings, PDA
from eligibility.constants import (
    WS_STATE_EX_CODE_ACA,
    WS_STATE_EX_CODE_NON_ACA,
    WS_FULL_TIME,
    )
from eligibility.core import InvalidStudentError
from eligibility.date_helper import determine_ccyys, determine_current_semester
import eligibility.models as models


class TestJobData(TestCase):
    def setUp(self):
        OJD = models.JobData
        self.original_job_model = OJD

        self.original_set_student_id = OJD.set_student_id
        self.original_set_job_family = OJD.set_job_family
        self.original_set_position_id = OJD.set_position_id
        self.original_set_job_classification = OJD.set_job_classification
        self.original_set_time_type = OJD.set_time_type
        self.original_set_scheduled_hours = OJD.set_scheduled_hours
        self.original_set_start_date = OJD.set_start_date
        self.original_set_end_date = OJD.set_end_date
        self.original_set_ccyys_to_check = OJD.set_ccyys_to_check
        self.original_set_max_work_hours = OJD.set_max_work_hours
        self.original_set_previous_appointments = OJD.set_previous_appointments

        OJD.set_student_id = mock.Mock(return_value=None)
        OJD.set_job_family = mock.Mock(return_value=None)
        OJD.set_position_id = mock.Mock(return_value=None)
        OJD.set_job_classification = mock.Mock(return_value=None)
        OJD.set_time_type = mock.Mock(return_value=None)
        OJD.set_scheduled_hours = mock.Mock(return_value=None)
        OJD.set_start_date = mock.Mock(return_value=None)
        OJD.set_end_date = mock.Mock(return_value=None)
        OJD.set_ccyys_to_check = mock.Mock(return_value=None)
        OJD.set_max_work_hours = mock.Mock(return_value=[None,None])
        OJD.set_previous_appointments = mock.Mock(return_value=[None,None])

    def tearDown(self):
        OJD = models.JobData
        OJD = self.original_job_model

        OJD.set_student_id = self.original_set_student_id
        OJD.set_job_family = self.original_set_job_family
        OJD.set_position_id = self.original_set_position_id
        OJD.set_job_classification = self.original_set_job_classification
        OJD.set_time_type = self.original_set_time_type
        OJD.set_scheduled_hours = self.original_set_scheduled_hours
        OJD.set_start_date = self.original_set_start_date
        OJD.set_end_date = self.original_set_end_date
        OJD.set_ccyys_to_check = self.original_set_ccyys_to_check
        OJD.set_max_work_hours = self.original_set_max_work_hours
        OJD.set_previous_appointments = self.original_set_previous_appointments

    def test_student_id(self):
        OJD = models.JobData
        OJD.set_student_id = self.original_set_student_id
        self.assertRaises(NotImplementedError, OJD)

    def test_job_family(self):
        OJD = models.JobData
        OJD.set_job_family = self.original_set_job_family
        self.assertRaises(NotImplementedError, OJD)

    def test_position_id(self):
        OJD = models.JobData
        OJD.set_position_id = self.original_set_position_id
        self.assertRaises(NotImplementedError, OJD)

    def test_job_classification(self):
        OJD = models.JobData
        OJD.set_job_classification = self.original_set_job_classification
        self.assertRaises(NotImplementedError, OJD)

    def test_time_type(self):
        OJD = models.JobData
        OJD.set_time_type = self.original_set_time_type
        self.assertRaises(NotImplementedError, OJD)

    def test_scheduled_hours(self):
        OJD = models.JobData
        OJD.set_scheduled_hours = self.original_set_scheduled_hours
        self.assertRaises(NotImplementedError, OJD)

    def test_start_date(self):
        OJD = models.JobData
        OJD.set_start_date = self.original_set_start_date
        self.assertRaises(NotImplementedError, OJD)

    def test_end_date(self):
        OJD = models.JobData
        OJD.set_end_date = self.original_set_end_date
        self.assertRaises(NotImplementedError, OJD)

    def test_in_boundary_non_aca(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Undergraduate Student Non-Academic'
            )
        OJD.set_max_work_hours = self.original_set_max_work_hours
        start_date = dt.strptime('20200801', '%Y%m%d')
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.strptime('20201005', '%Y%m%d')
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertTrue(job.is_in_boundary)

    def test_not_in_boundary_non_aca(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Undergraduate Student Non-Academic'
            )
        OJD.set_max_work_hours = self.original_set_max_work_hours
        start_date = dt.strptime('20201001', '%Y%m%d')
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.strptime('20200105', '%Y%m%d')
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertFalse(job.is_in_boundary)

    def test_in_boundary_aca(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Undergraduate Student Academic'
            )
        OJD.set_max_work_hours = self.original_set_max_work_hours
        start_date = dt.strptime('20200801', '%Y%m%d')
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.strptime('20200901', '%Y%m%d')
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertTrue(job.is_in_boundary)

    def test_not_in_boundary_aca(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Undergraduate Student Academic'
            )
        start_date = dt.strptime('20201001', '%Y%m%d')
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.strptime('20210105', '%Y%m%d')
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertFalse(job.is_in_boundary)

    def test_not_in_boundary_in_same_bucket(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Undergraduate Student Academic'
            )
        OJD.set_max_work_hours = self.original_set_max_work_hours
        start_date = dt.strptime('20200801', '%Y%m%d')
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.strptime('20200815', '%Y%m%d')
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertFalse(job.is_in_boundary)

    def test_not_in_boundary_dec_jan(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Undergraduate Student Non-Academic'
            )
        OJD.set_max_work_hours = self.original_set_max_work_hours
        start_date = dt.strptime('20201201', '%Y%m%d')
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.strptime('20210105', '%Y%m%d')
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertFalse(job.is_in_boundary)

    def test_in_boundary_in_different_buckets(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Undergraduate Student Academic'
            )
        OJD.set_max_work_hours = self.original_set_max_work_hours
        start_date = dt.strptime('20200801', '%Y%m%d')
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.strptime('20201215', '%Y%m%d')
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertTrue(job.is_in_boundary)


    def test_ccyys_to_check_diff_overlaps(self):
        OJD = models.JobData
        OJD.set_ccyys_to_check = self.original_set_ccyys_to_check
        start_date = dt.strptime('20180815', '%Y%m%d')
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.strptime('20190831', '%Y%m%d')
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertEqual(job.ccyys_to_check, '20196')
        self.assertNotEqual(job.ccyys_to_check, '20199')

    def test_ccyys_to_check_retro(self):
        OJD = models.JobData
        OJD.set_ccyys_to_check = self.original_set_ccyys_to_check
        start_date = dt.now() - timedelta(days=400)
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.now() - timedelta(days=365)
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertEqual(job.ccyys_to_check, determine_ccyys(end_date))
        self.assertNotEqual(job.ccyys_to_check, determine_current_semester())

    def test_ccyys_to_check_future(self):
        OJD = models.JobData
        OJD.set_ccyys_to_check = self.original_set_ccyys_to_check
        start_date = dt.now() + timedelta(days=100)
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.now() + timedelta(days=200)
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertEqual(job.ccyys_to_check, determine_ccyys(start_date))

    def test_ccyys_to_check_now(self):
        OJD = models.JobData
        OJD.set_ccyys_to_check = self.original_set_ccyys_to_check
        start_date = dt.now()
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.now() + timedelta(days=10)
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertEqual(job.ccyys_to_check, determine_current_semester())

    def test_ccyys_to_check_not_now(self):
        OJD = models.JobData
        OJD.set_ccyys_to_check = self.original_set_ccyys_to_check
        start_date = dt.now() - timedelta(days=40)
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.now() + timedelta(days=10)
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertEqual(job.ccyys_to_check, determine_current_semester())

    def test_set_ajobs(self):
        OJD = models.JobData
        job = OJD()
        self.assertRaises(NotImplementedError, job.set_ajobs)

    def test_max_work_hours_non_aca(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Undergraduate Student Non-Academic'
            )
        OJD.set_max_work_hours = self.original_set_max_work_hours
        start_date = dt.now()
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.now() + timedelta(days=60)
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertEqual(job.max_work_hours, 40)
        self.assertEqual(job.total_work_hours, 40)

    def test_max_work_hours_aca(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Undergraduate Student Academic'
            )
        OJD.set_max_work_hours = self.original_set_max_work_hours
        start_date = dt.now()
        OJD.set_start_date = mock.Mock(return_value=start_date)
        end_date = dt.now() + timedelta(days=60)
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertEqual(job.max_work_hours, 20)
        self.assertEqual(job.total_work_hours, 20)

    def test_max_work_hours_ws_aca(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Work-Study'
            )
        OJD.set_job_classification = mock.Mock(
            return_value=WS_STATE_EX_CODE_ACA
            )
        OJD.set_time_type = mock.Mock(return_value=None)
        OJD.set_max_work_hours = self.original_set_max_work_hours
        job = OJD()
        self.assertEqual(job.max_work_hours, 19)
        self.assertEqual(job.total_work_hours, 20)

    def test_max_work_hours_ws_aca_ft(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Work-Study'
            )
        OJD.set_job_classification = mock.Mock(
            return_value=WS_STATE_EX_CODE_ACA
            )
        OJD.set_time_type = mock.Mock(return_value=WS_FULL_TIME)
        OJD.set_max_work_hours = self.original_set_max_work_hours
        job = OJD()
        self.assertEqual(job.max_work_hours, 40)
        self.assertEqual(job.total_work_hours, 40)

    def test_max_work_hours_ws_non_aca(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Work-Study'
            )
        OJD.set_job_classification = mock.Mock(
            return_value=WS_STATE_EX_CODE_NON_ACA
            )
        OJD.set_time_type = mock.Mock(return_value=None)
        OJD.set_max_work_hours = self.original_set_max_work_hours
        job = OJD()
        self.assertEqual(job.max_work_hours, 19)
        self.assertEqual(job.total_work_hours, 40)

    def test_max_work_hours_ws_non_aca_ft(self):
        OJD = models.JobData
        OJD.set_job_family = mock.Mock(
            return_value='Work-Study'
            )
        OJD.set_job_classification = mock.Mock(
            return_value=WS_STATE_EX_CODE_NON_ACA
            )
        OJD.set_time_type = mock.Mock(return_value=WS_FULL_TIME)
        OJD.set_max_work_hours = self.original_set_max_work_hours
        job = OJD()
        self.assertEqual(job.max_work_hours, 40)
        self.assertEqual(job.total_work_hours, 40)

    def test_previous_appointments(self):
        OJD = models.JobData
        OJD.set_previous_appointments = self.original_set_previous_appointments
        self.assertRaises(NotImplementedError, OJD)

    def test_is_retro_start_true(self):
        OJD = models.JobData
        start_date = dt.now() - timedelta(days=10)
        OJD.set_start_date = mock.Mock(return_value=start_date)
        job = OJD()
        self.assertTrue(job.is_retro_start)

    def test_is_retro_start_false(self):
        OJD = models.JobData
        start_date = dt.now() + timedelta(days=10)
        OJD.set_start_date = mock.Mock(return_value=start_date)
        job = OJD()
        self.assertFalse(job.is_retro_start)

    def test_is_retro_end_true(self):
        OJD = models.JobData
        end_date = dt.now() - timedelta(days=10)
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertTrue(job.is_retro_end)

    def test_is_retro_end_false(self):
        OJD = models.JobData
        end_date = dt.now() + timedelta(days=10)
        OJD.set_end_date = mock.Mock(return_value=end_date)
        job = OJD()
        self.assertFalse(job.is_retro_end)

    def test_is_future_hire_true(self):
        OJD = models.JobData
        start_date = dt.now() - timedelta(days=10)
        OJD.set_start_date = mock.Mock(return_value=start_date)
        job = OJD()
        self.assertFalse(job.is_future_hire)

    def test_is_future_hire_false(self):
        OJD = models.JobData
        start_date = dt.now() + timedelta(days=10)
        OJD.set_start_date = mock.Mock(return_value=start_date)
        job = OJD()
        self.assertTrue(job.is_future_hire)


class TestStudentData(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.student_id = 'abc123'
        self.job.additional_jobs = []
        models.settings = TestSettings()
        self.original_mf_data = MF_DATA
        self.original_pda = PDA
        OSD = models.StudentData
        self.original_student_model = OSD
        self.original_load_mainframe_data = OSD.load_mainframe_data
        OSD.load_mainframe_data = mock.Mock(return_value=self.original_mf_data)

    def tearDown(self):
        OSD = models.StudentData
        OSD = self.original_student_model
        OSD.load_mainframe_data = self.original_load_mainframe_data
        MF_DATA = self.original_mf_data
        PDA = self.original_pda

    def test_set_age(self):
        OSD = models.StudentData
        student = OSD({},self.job)
        self.assertEqual(student.age, 16)

    def test_set_under_age(self):
        OSD = models.StudentData
        models.settings.DEBUG = True
        models.settings.UNDER_AGE = [self.job.student_id]
        student = OSD({},self.job)
        self.assertEqual(student.age, 14)
        models.settings.DEBUG = False
        models.settings.UNDER_AGE = []

    def test_set_residency_domestic_1(self):
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        student = OSD(PDA, self.job)
        self.assertEqual(student.residency, 'D')

    def test_set_residency_domestic_2(self):
        PDA.recv.common_fields.citizenship = '3'
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        student = OSD(PDA, self.job)
        self.assertEqual(student.residency, 'D')
        PDA.recv.common_fields.citizenship = '0'

    def test_set_residency_foreign(self):
        PDA.recv.common_fields.citizenship = '2'
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        student = OSD(PDA, self.job)
        self.assertEqual(student.residency, 'F')
        PDA.recv.common_fields.citizenship = '0'

    def test_is_grad_student_true(self):
        MF_DATA['grad_status'] = 'grad'
        OSD = models.StudentData
        student = OSD({}, self.job)
        self.assertTrue(student.is_grad_student())
        MF_DATA['grad_status'] = 'undergrad'

    def test_is_grad_student_false(self):
        OSD = models.StudentData
        student = OSD({}, self.job)
        self.assertFalse(student.is_grad_student())

    def test_is_undergrad_true(self):
        OSD = models.StudentData
        student = OSD({}, self.job)
        self.assertTrue(student.is_undergrad())

    def test_is_undergrad_false(self):
        MF_DATA['grad_status'] = 'grad'
        OSD = models.StudentData
        student = OSD({}, self.job)
        self.assertFalse(student.is_undergrad())
        MF_DATA['grad_status'] = 'undergrad'

    def test_invalid_hr_deadling(self):
        PDA.recv.semester_data.hr_deadline = 'foobar'
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        self.assertRaises(ValueError, OSD, PDA, self.job)
        PDA.recv.semester_data.hr_deadline = '09012019'

    def test_set_grad_status_grad(self):
        PDA.recv.common_fields.graduate_student = True
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        student = OSD(PDA, self.job)
        self.assertTrue(student.is_grad_student())
        PDA.recv.common_fields.graduate_student = False

    def test_set_grad_status_undergrad(self):
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        student = OSD(PDA, self.job)
        self.assertTrue(student.is_undergrad())

    def test_invalid_birthdate(self):
        PDA.recv.common_fields.birthdate_ccyymmdd = 'foobar'
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        self.assertRaises(InvalidStudentError, OSD, PDA, self.job)
        PDA.recv.common_fields.birthdate_ccyymmdd = '19901010'

    def test_is_on_exception_table(self):
        PDA.recv.common_fields.student_exception = True
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        student = OSD(PDA, self.job)
        self.assertTrue(student.is_on_exception_table())
        PDA.recv.common_fields.student_exception = False

    def test_is_not_on_exception_table(self):
        PDA.recv.common_fields.student_exception = False
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        student = OSD(PDA, self.job)
        self.assertFalse(student.is_on_exception_table())

    def test_has_additional_jobs(self):
        self.job.additional_jobs = ['job1', 'job2']
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        student = OSD(PDA, self.job)
        self.assertTrue(student.has_additional_jobs())
        self.job.additional_jobs = []

    def test_has_no_additional_jobs(self):
        OSD = models.StudentData
        OSD.load_mainframe_data = self.original_load_mainframe_data
        student = OSD(PDA, self.job)
        self.assertFalse(student.has_additional_jobs())

