from datetime import datetime as dt, timedelta
from unittest import TestCase, mock

from .data import Job
import eligibility.date_helper as dh


class StartsInBoundary(TestCase):
    def test_spring_boundary_1(self):
        date = dt.strptime('20201201', '%Y%m%d')
        self.assertTrue(dh.in_boundary(date))

    def test_spring_boundary_2(self):
        date = dt.strptime('20200115', '%Y%m%d')
        self.assertTrue(dh.in_boundary(date))

    def test_summer_boundary_1(self):
        date = dt.strptime('20200501', '%Y%m%d')
        self.assertTrue(dh.in_boundary(date))

    def test_summer_boundary_2(self):
        date = dt.strptime('20200531', '%Y%m%d')
        self.assertTrue(dh.in_boundary(date))

    def test_fall_boundary_1(self):
        date = dt.strptime('20200801', '%Y%m%d')
        self.assertTrue(dh.in_boundary(date))

    def test_fall_boundary_2(self):
        date = dt.strptime('20200831', '%Y%m%d')
        self.assertTrue(dh.in_boundary(date))

    def test_out_of_bounds(self):
        date = dt.strptime('20200901', '%Y%m%d')
        self.assertFalse(dh.in_boundary(date))
        date = dt.strptime('20201130', '%Y%m%d')
        self.assertFalse(dh.in_boundary(date))
        date = dt.strptime('20200116', '%Y%m%d')
        self.assertFalse(dh.in_boundary(date))
        date = dt.strptime('20200430', '%Y%m%d')
        self.assertFalse(dh.in_boundary(date))
        date = dt.strptime('20200601', '%Y%m%d')
        self.assertFalse(dh.in_boundary(date))
        date = dt.strptime('20200731', '%Y%m%d')
        self.assertFalse(dh.in_boundary(date))


class DetermineSemester(TestCase):
    def test_spring(self):
        spring_day = dt.strptime('20190310', '%Y%m%d')
        result = dh.determine_semester(spring_day)
        self.assertEqual(result, 2)

    def test_spring_edge(self):
        spring_day = dt.strptime('20190116', '%Y%m%d')
        result = dh.determine_semester(spring_day)
        self.assertEqual(result, 2)

    def test_summer(self):
        summer_day = dt.strptime('20190710', '%Y%m%d')
        result = dh.determine_semester(summer_day)
        self.assertEqual(result, 6)

    def test_summer_edge(self):
        summer_day = dt.strptime('20190601', '%Y%m%d')
        result = dh.determine_semester(summer_day)
        self.assertEqual(result, 6)

    def test_fall_jan(self):
        fall_day = dt.strptime('20200110', '%Y%m%d')
        result = dh.determine_semester(fall_day)
        self.assertEqual(result, 9)

    def test_fall_edge(self):
        fall_day = dt.strptime('20190901', '%Y%m%d')
        result = dh.determine_semester(fall_day)
        self.assertEqual(result, 9)

    def test_spring_in_boundary(self):
        day = dt.strptime('20191201', '%Y%m%d')
        result = dh.determine_semester(day, in_boundary=True)
        self.assertEqual(result, 2)

    def test_summer_in_boundary(self):
        day = dt.strptime('20190501', '%Y%m%d')
        result = dh.determine_semester(day, in_boundary=True)
        self.assertEqual(result, 6)

    def test_fall_in_boundary(self):
        day = dt.strptime('20190801', '%Y%m%d')
        result = dh.determine_semester(day, in_boundary=True)
        self.assertEqual(result, 9)

    def test_is_ws_early_aug(self):
        day = dt.strptime('20190801', '%Y%m%d')
        result = dh.determine_semester(day, in_boundary=True, work_study=True)
        self.assertEqual(result, 6)

    def test_is_ws_late_aug(self):
        day = dt.strptime('20190816', '%Y%m%d')
        result = dh.determine_semester(day, in_boundary=True, work_study=True)
        self.assertEqual(result, 9)

    def test_is_ws_early_jan(self):
        day = dt.strptime('20190101', '%Y%m%d')
        result = dh.determine_semester(day, in_boundary=True, work_study=True)
        self.assertEqual(result, 2)


class DetermineSummerSemester(TestCase):
    def test_summer_1(self):
        summer_day = dt.strptime('20190710', '%Y%m%d')
        result = dh.determine_summer_semester(summer_day, '20196')
        self.assertEqual(result, '6-1')

    def test_summer_2(self):
        summer_day = dt.strptime('20190720', '%Y%m%d')
        result = dh.determine_summer_semester(summer_day, '20196')
        self.assertEqual(result, '6-2')

    def test_invalid_summer_date(self):
        non_summer_day = dt.strptime('20190220', '%Y%m%d')
        self.assertRaises(
            dh.InvalidSummerDateError,
            dh.determine_summer_semester, non_summer_day, '20192',
            )


class DetermineCCYS(TestCase):
    def test_spring(self):
        spring_day = dt.strptime('20190310', '%Y%m%d')
        result = dh.determine_ccyys(spring_day)
        self.assertEqual(result, '20192')

    def test_summer(self):
        summer_day = dt.strptime('20190710', '%Y%m%d')
        result = dh.determine_ccyys(summer_day)
        self.assertEqual(result, '20196')

    def test_fall_1(self):
        fall_day = dt.strptime('20200110', '%Y%m%d')
        result = dh.determine_ccyys(fall_day)
        self.assertEqual(result, '20199')

    def test_fall_2(self):
        fall_day = dt.strptime('20191001', '%Y%m%d')
        result = dh.determine_ccyys(fall_day)
        self.assertEqual(result, '20199')

    def test_fall_spring_overlap_december(self):
        overlap_day = dt.strptime('20191201', '%Y%m%d')
        result = dh.determine_ccyys(overlap_day, in_boundary=True)
        self.assertEqual(result, '20202')


class JobsOverlap(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.start_date = dt.strptime('20190720', '%Y%m%d')
        self.job.end_date = dt.strptime('20190730', '%Y%m%d')
        self.addl_job = {
            'start_date': dt.strptime('20190720', '%Y%m%d'),
            'end_date': dt.strptime('20190730', '%Y%m%d'),
            }

    def test_overlap_1(self):
        """jobs are completely alligned"""
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_overlap_2(self):
        """job starts before addl_job"""
        self.job.start_date = dt.strptime('20190710', '%Y%m%d')
        self.addl_job['end_date'] = dt.strptime('20190830', '%Y%m%d')
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_overlap_3(self):
        """job starts after addl_job"""
        self.job.start_date = dt.strptime('20190725', '%Y%m%d')
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_overlap_4(self):
        """job completely overlaps addl_job"""
        self.job.start_date = dt.strptime('20190501', '%Y%m%d')
        self.job.end_date = dt.strptime('20191001', '%Y%m%d')
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_overlap_5(self):
        """job completely within addl_job"""
        self.job.start_date = dt.strptime('20190721', '%Y%m%d')
        self.job.end_date = dt.strptime('20190725', '%Y%m%d')
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_no_overlap(self):
        self.job.start_date = dt.strptime('20190201', '%Y%m%d')
        self.job.end_date = dt.strptime('20190401', '%Y%m%d')
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertFalse(result)

