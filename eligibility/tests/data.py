from datetime import datetime as dt, timedelta
import factory

from eligibility.models import JobData, StudentData


class StudentFactory(factory.Factory):
    class Meta:
        model = StudentData

    current_sem_status = 2


class JobFactory(factory.Factory):
    class Meta:
        model = JobData


class Student(object):
    student_id = 'abc123'
    grad_data = {'eligible':True, 'failed_audits':[]}

    def is_us_citizen(self):
        return True


class Job(object):
    student_id = 'abc123'
    job_family = 'Work Study'


class Validator(object):
    eligibility = 1
    criticality = 'Warning'
    messages = ['Fakey.', 'Fake.', 'Messages.']
    statuses = ['crit', 'non-crit', 'non-crit']

    def __init__(self, job, student):
        pass

    def validate_assignment(self):
        pass


class SoapRequest(object):
    def __init__(self, url, username, password, nsmap):
        pass

    def append_to_body(self, xml):
        pass


class Response(object):
    def __init__(self, response_xml):
        self.text = response_xml


GOOD_RESPONSE_XML = """
<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
   <env:Body>
      <wd:Server_Timestamp wd:version="v30.0" xmlns:wd="urn:com.workday/bsvc">
         <wd:Server_Timestamp_Data>2018-12-04T14:07:52.894-08:00</wd:Server_Timestamp_Data>
      </wd:Server_Timestamp>
   </env:Body>
</env:Envelope>
"""


FAIL_RESPONSE_XML = """
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
   <SOAP-ENV:Body>
      <SOAP-ENV:Fault xmlns:wd="urn:com.workday/bsvc">
         <faultcode>SOAP-ENV:Client.authenticationError</faultcode>
         <faultstring>tenant is temporarily unavailable due to server patching</faultstring>
      </SOAP-ENV:Fault>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
"""


class TestSettings(object):
    WD_API_VERSION = 'v30.0'
    WD_API_URL = 'https://'
    WD_USER = 'ISU123456'
    WD_PASS = '**********'
    DEBUG = False
    UNDER_AGE = False


MF_DATA = {
    'hr_deadline': None,
    'grad_status': 'undergrad',
    'birthdate': dt.now() - timedelta(days=6000),
    'gpa': 0,
    'residency': 'D',
    'current_sem_hours': 0,
    'current_sem_status': '',
    'prev_sem_hours': 0,
    'prev_sem_status': '',
    'fall_hours': 0,
    'fall_status': '',
    'graduation_date': '',
    'degree_seeking': False,
    'student_exception': False,
    'will_return_for_fall': False,
    'grad_data':{'eligible': False, 'failed_audits': []},
    'work_study': {
        'ws_error_returned': False,
        'ok_to_disburse': False,
        'disbursement_msg': ''
        },
}


class SemesterData(object):
    hr_deadline = '09012019'


class CommonFields(object):
    graduate_student = False
    birthdate_ccyymmdd = '19901010'
    cumulative_gpa = '3.0'
    citizenship = '0'
    current_semester_hours = 10
    current_semester_status = ''
    previous_semester_hours = 10
    previous_semester_status = ''
    fall_semester_hours = 10
    fall_semester_status = ''
    graduation_date = ''
    degree_seeking = 'Y'
    student_exception = 'Y'
    will_return_for_fall = 'Y'


class GSFailedAudit(object):
    def __init__(self, critical, number):
        self.is_critical = critical
        self.failed_audit_msg = 'Failed GS audit {0}.'.format(number)


class GSFields(object):
    eligible_for_position = 'Y'
    failed_audit_array = []


class WSFields(object):
    ws_error_returned = 'Y'
    ok_to_disburse = 'Y'
    disbursement_msg = ''


class Recv(object):
    semester_data = SemesterData()
    common_fields = CommonFields()
    grad_student_fields = GSFields()
    work_study_fields = WSFields


class PDA(object):
    recv = Recv()

