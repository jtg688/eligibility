from datetime import datetime as dt, timedelta
from unittest import TestCase, mock

from .data import (
    GSFailedAudit,
    FAIL_RESPONSE_XML,
    GOOD_RESPONSE_XML,
    Job,
    Response,
    SoapRequest,
    Student,
    TestSettings,
    Validator,
    )
from eligibility.constants import *
import eligibility.core as core


class DetermineEligibility(TestCase):
    def setUp(self):
        self.original_GSJobValidator = core.GSJobValidator
        self.original_UGNAValidator = core.UGNAValidator
        self.original_UGANTValidator = core.UGANTValidator
        self.original_WSNonAcaValidator = core.WSNonAcaValidator
        self.original_WSAcaValidator = core.WSAcaValidator
        self.student = Student()
        self.student.is_on_exception_table = mock.Mock(return_value=False)
        self.job = Job()
        self.job.job_profile = 'U101'

    def tearDown(self):
        core.GSJobValidator = self.original_GSJobValidator
        core.UGNAValidator = self.original_UGNAValidator
        core.UGANTValidator = self.original_UGANTValidator
        core.WSNonAcaValidator = self.original_WSNonAcaValidator
        core.WSAcaValidator = self.original_WSAcaValidator

    def test_on_exception_table(self):
        self.student.is_on_exception_table = mock.Mock(return_value=True)
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(result['warning_level'], ['non-crit'])
        self.assertEqual(result['msg'], ['Student is on exception table.'])

    def test_grad_student(self):
        self.job.job_profile = 'G101'
        self.job.is_retro_start = mock.Mock(return_value=False)
        failed_audits = [GSFailedAudit(True, 1), GSFailedAudit(False, 2)]
        self.student.grad_data['eligible'] = False
        self.student.grad_data['failed_audits'] = failed_audits
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 0)
        self.assertEqual(result['warning_level'], ['crit', 'non-crit'])
        self.assertEqual(
            result['msg'],
            ['Failed GS audit 1.', 'Failed GS audit 2.'],
            )
        self.student.grad_data = {}

    def test_non_aca(self):
        core.UGNAValidator = Validator
        self.job.job_family = 'Undergraduate Student Non-Academic'
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(
            result['warning_level'],
            ['crit', 'non-crit', 'non-crit'],
            )
        self.assertEqual(result['msg'],['Fakey.', 'Fake.', 'Messages.'])

    def test_aca(self):
        core.UGANTValidator = Validator
        self.job.job_family = 'Undergraduate Student Academic'
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(
            result['warning_level'],
            ['crit', 'non-crit', 'non-crit'],
            )
        self.assertEqual(result['msg'],['Fakey.', 'Fake.', 'Messages.'])

    def test_ws_non_aca(self):
        core.WSNonAcaValidator = Validator
        self.job.job_family = 'Work-Study'
        self.job.job_classification = WS_STATE_EX_CODE_NON_ACA
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(
            result['warning_level'],
            ['crit', 'non-crit', 'non-crit'],
            )
        self.assertEqual(result['msg'],['Fakey.', 'Fake.', 'Messages.'])

    def test_ws_aca(self):
        core.WSAcaValidator = Validator
        self.job.job_family = 'Work-Study'
        self.job.job_classification = WS_STATE_EX_CODE_ACA
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(
            result['warning_level'],
            ['crit', 'non-crit', 'non-crit'],
            )
        self.assertEqual(result['msg'],['Fakey.', 'Fake.', 'Messages.'])

    def test_invalid_ws_classification(self):
        core.WSAcaValidator = Validator
        self.job.job_family = 'Work-Study'
        self.job.job_classification = 'foobar'
        self.assertRaises(
            core.InvalidJobProfileError,
            core.determine_eligibility,
            self.job, self.student,
            )

    def test_invalid_job_family(self):
        self.job.job_family = 'foobar'
        self.assertRaises(
            core.InvalidJobProfileError,
            core.determine_eligibility,
            self.job, self.student,
            )

