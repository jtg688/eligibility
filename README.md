README
=====

What is this repository for?
----

This is the core SEE library which is share by [SEE Batch (APP007)](https://bitbucket.org/utasmpint/app007/src/master/)
and the [SEE Online API](https://github.austin.utexas.edu/asmp-int/se_api).

How do I get set up?
----

In order to work with this library, after cloning it locally, create a virtual env
`python3 -m venv /path/to/new/virtual/environment`. Fire up the venv `source /path/to/new/virtual/environment`, 
then `pip install test_requirements.txt`. You can now use `pytest` to run the existing test suite and validate
your changes. In order to see the testing coverage level, run `pytest --cov`.

### Testing in apps ###
Once you have changes you would like to validate in the context of one or both of the SEE apps, you can do so by 
running either app locally. After installing their requirements, simply run `pip install /path/to/local/eligibility/library`.

Contribution guidelines
----

Tests are required for any changes you contribute. The current coverage level is at least 98%, and the expectation
is that your contributions will not reduce this coverage level. 

### Migration ###

__*Important*__  
Changes to this library must be pulled into both SEE Batch and SEE Online via their respective requirements.txt files.
Therefore testing of any changes to this library must be tested in the context of both applications.

Once a change has been tested and approved, create a new tag of this library and update the requirements.txt of both
SEE applications to pull in that tag.

Who do I talk to?
----

* Repo owner or admin
* Todd is the SEE code expert