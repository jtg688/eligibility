from setuptools import setup

setup(name='eligibility',
      version='0.1',
      description='The student eligibility engine.',
      url='https://bitbucket.org/jtg688/eligibility/src/master/',
      author='Todd Graves',
      author_email='tgraves@austin.utexas.edu',
      license='MIT',
      packages=['eligibility'],
      zip_safe=False)
